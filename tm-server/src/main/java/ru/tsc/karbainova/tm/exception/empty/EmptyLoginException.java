package ru.tsc.karbainova.tm.exception.empty;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {
    public EmptyLoginException() {
        super("Error Login.");
    }

    public EmptyLoginException(String value) {
        super("Error Login. " + value);
    }
}
