package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.api.service.dto.IAdminUserService;
import ru.tsc.karbainova.tm.api.service.dto.IProjectService;
import ru.tsc.karbainova.tm.api.service.dto.IProjectToTaskService;
import ru.tsc.karbainova.tm.api.service.dto.ISessionService;
import ru.tsc.karbainova.tm.api.service.dto.ITaskService;
import ru.tsc.karbainova.tm.api.service.dto.IUserService;

public interface ServiceLocator {
    ITaskService getTaskService();

    IProjectService getProjectService();

    IUserService getUserService();

    ISessionService getSessionService();

    IAdminUserService getAdminUserService();

    IProjectToTaskService getProjectToTaskService();
}
