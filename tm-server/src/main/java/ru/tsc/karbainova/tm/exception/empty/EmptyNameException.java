package ru.tsc.karbainova.tm.exception.empty;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {
    public EmptyNameException() {
        super("Error Name.");
    }
}
