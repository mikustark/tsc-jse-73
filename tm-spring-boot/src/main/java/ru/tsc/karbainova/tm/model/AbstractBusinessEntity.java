package ru.tsc.karbainova.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.karbainova.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractBusinessEntity extends AbstractEntity{

    protected Date created = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date endDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date startDate;

    protected String description = "";

    protected String name = "";

    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Column(name = "user_id")
    protected String userId;

}
